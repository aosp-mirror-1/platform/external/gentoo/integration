LOCAL_PATH := $(call my-dir)

PORTAGE_PREFIX := $(LOCAL_PATH)/../../portage
BINDIR := $(PORTAGE_PREFIX)/usr/bin

define CREATE_WRAPPER
	$(hide)mkdir -p $(dir $@)
	$(hide)sed \
		-e 's:@ROOT_SUBDIR@:$(3RD_PARTY_ROOT_SUBDIR):g' \
		-e 's:@BIN@:$(BINDIR)/$(notdir $@):' \
		$< > $@.tmp \
		&& chmod a+rx $@.tmp && mv $@.tmp $@
endef

CBUILD = $(CLANG_CONFIG_$(HOST_ARCH)_HOST_TRIPLE)
CHOST  = $(CLANG_CONFIG_$(TARGET_ARCH)_TARGET_TRIPLE)

# Map android's arch naming to Gentoo's.
ANDROID_ARCH-mips64 = mips
ANDROID_ARCH-x86_64 = amd64
gentoo-arch = $(if $(ANDROID_ARCH-$(1)),$(ANDROID_ARCH-$(1)),$(1))
HOST_KEYWORD   := $(call gentoo-arch,$(HOST_ARCH))
TARGET_KEYWORD := $(call gentoo-arch,$(TARGET_ARCH))

# Common build doesn't set this up.
TARGET_RANLIB ?= $(TARGET_AR:ar=ranlib)

MAIN_OVERLAY_REPO := `readlink -f $(LOCAL_PATH)`/../../overlays/gentoo

include $(CLEAR_VARS)

#
# Create /etc/portage config files.
#

make.profile := $(3RD_PARTY_ROOT)/etc/portage/make.profile
$(make.profile):
	$(hide)mkdir -p $(dir $@)
	$(hide)ln -sfT \
		$(MAIN_OVERLAY_REPO)/profiles/default/linux/$(TARGET_KEYWORD) \
		$@

LOCAL_MODULE := make.conf
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

make.conf := $(intermediates)/make.conf
$(make.conf): $(LOCAL_PATH)/make.conf.in $(make.profile)
	$(hide)mkdir -p $(dir $@)
	$(hide)sed \
		-e 's:@CC@:$(notdir $(3RD_PARTY_CC)):g' \
		-e 's:@CXX@:$(notdir $(3RD_PARTY_CXX)):g' \
		-e 's:@AR@:$(notdir $(TARGET_AR)):g' \
		-e 's:@RANLIB@:$(notdir $(TARGET_RANLIB)):g' \
		-e 's:@LD@:$(notdir $(TARGET_LD)):g' \
		-e 's:@OBJCOPY@:$(notdir $(TARGET_OBJCOPY)):g' \
		-e 's:@STRIP@:$(notdir $(TARGET_STRIP)):g' \
		-e 's:@READELF@:$(notdir $(TARGET_READELF)):g' \
		-e 's:@PKG_CONFIG@:$(notdir $(3RD_PARTY_PKG_CONFIG)):g' \
		-e 's:@ARCH@:$(TARGET_ARCH):g' \
		-e 's:@CBUILD@:$(CBUILD):g' \
		-e 's:@CHOST@:$(CHOST):g' \
		-e 's:@COMMON_OBJ@:$(if $(filter /%,$(TARGET_OUT_COMMON_GENTOO)),,$${ANDROID_BUILD_TOP}/)$(TARGET_OUT_COMMON_GENTOO):g' \
		-e 's:@PRODUCT_OBJ@:$${ANDROID_PRODUCT_OUT}/obj/gentoo:g' \
		-e 's:@PORTAGE_PREFIX@:$(PORTAGE_PREFIX):g' \
		-e 's:@LIBDIR@:$(3RD_PARTY_LIBDIR):' \
		$< > $@

include $(CLEAR_VARS)

LOCAL_MODULE := package.provided
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage/profile
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

package.provided := $(intermediates)/package.provided
$(package.provided): $(LOCAL_PATH)/package.provided.in
	$(hide)mkdir -p $(dir $@)
	$(hide)cp $< $@

include $(CLEAR_VARS)

LOCAL_MODULE := make.defaults
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage/profile
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

make.defaults := $(intermediates)/make.defaults
$(make.defaults): $(LOCAL_PATH)/make.defaults.in
	$(hide)mkdir -p $(dir $@)
	$(hide)cp $< $@

include $(CLEAR_VARS)

LOCAL_MODULE := package.use
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage/profile
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

package.use := $(intermediates)/$(LOCAL_MODULE)
$(package.use): $(LOCAL_PATH)/$(LOCAL_MODULE).in
	$(hide)mkdir -p $(dir $@)
	$(hide)cp $< $@

include $(CLEAR_VARS)

LOCAL_MODULE := use.force
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage/profile
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

use.force := $(intermediates)/$(LOCAL_MODULE)
$(use.force): $(LOCAL_PATH)/$(LOCAL_MODULE).in
	$(hide)mkdir -p $(dir $@)
	$(hide)cp $< $@

include $(CLEAR_VARS)

LOCAL_MODULE := use.mask
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage/profile
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

use.mask := $(intermediates)/$(LOCAL_MODULE)
$(use.mask): $(LOCAL_PATH)/$(LOCAL_MODULE).in
	$(hide)mkdir -p $(dir $@)
	$(hide)cp $< $@

include $(CLEAR_VARS)

LOCAL_MODULE := gentoo.conf
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(3RD_PARTY_ROOT)/etc/portage/repos.conf
3RD_PARTY_CONFIGS += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

gentoo.conf := $(intermediates)/gentoo.conf
$(gentoo.conf): $(LOCAL_PATH)/repos.conf.in
	$(hide)mkdir -p $(dir $@)
	$(hide)sed -e "s:@OVERLAY_LOCATION@:$(MAIN_OVERLAY_REPO):" $< > $@

#
# Create various bin/ wrappers.
# XXX: These contain product specific paths and should not.
#

include $(CLEAR_VARS)

LOCAL_MODULE := ebuild
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true
3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

ebuild := $(intermediates)/$(LOCAL_MODULE)
$(ebuild): $(LOCAL_PATH)/wrapper.in
	$(CREATE_WRAPPER)

LOCAL_BUILT_MODULE := $(ebuild)

include $(CLEAR_VARS)

LOCAL_MODULE := emerge
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true
3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

LOCAL_BUILT_MODULE := $(intermediates)/$(LOCAL_MODULE)
$(LOCAL_BUILT_MODULE): $(LOCAL_PATH)/wrapper.in | $(patsubst %,$(HOST_OUT_EXECUTABLES)/%,ebuild portageq)
	$(CREATE_WRAPPER)

include $(CLEAR_VARS)

LOCAL_MODULE := portageq
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true
3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

portageq := $(intermediates)/portageq
$(portageq): $(LOCAL_PATH)/wrapper.in
	$(CREATE_WRAPPER)

LOCAL_BUILT_MODULE := $(portageq)

include $(CLEAR_VARS)

LOCAL_MODULE := 3rd-party-merge
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true
3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)

include $(BUILD_SYSTEM)/base_rules.mk

LOCAL_BUILT_MODULE := $(intermediates)/$(LOCAL_MODULE)
$(LOCAL_BUILT_MODULE): $(LOCAL_PATH)/wrapper.in
	$(CREATE_WRAPPER)
